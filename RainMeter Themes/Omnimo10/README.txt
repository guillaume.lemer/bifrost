Omnimo 10 by Xyrfo and fediaFedia
Original Release: 16th of May 2020

Update: January 2022: Fixed Weather


10.0.3 Release: 26th of August 2020
Fixed Weather, Added DarkMode Panel, More Scaling options (75% - 225%), New Wallpaper, Tweaks to HDD, Wifi, also added Turkish language.

10.0.2 Release: 24th of May 2020
Fixes: Consolidated exe files into just two, compiled for 64bit (32bit users, please download a hotfix here: omnimo.info/32)

NEW USERS / CLEAN INSTALL

If you have Rainmeter installed, just double click Omnimo.rmskin
If not, launch Setup.exe and follow the instructions to download Rainmeter, after you've installed it, Omnimo will be installed by the installer.


ATTENTION! EXISTING USERS!

You might want to make a backup of your WP7 folder just in case. The new version will overwrite all your Omnimo files and settings. 
Since this is a major release, we cannot offer any upgrade options.

Navigate to your Documents\Rainmeter\Skins folder and rename WP7 to something else, for instance, WP7_backup before installing.

--

Please visit our website at http://omnimo.info for more info

PERFORMANCE NOTE: The Blur plugin for Windows 10 rarely causes performance issues. 
If you feel that everything lags, try disabling Blur either from Settings or from the Extras folder here. 